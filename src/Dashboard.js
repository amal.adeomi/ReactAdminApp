import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import PieChart from './elements/charts/piechart'
import LineChart from './elements/charts/linechart'
import AreaChart from './elements/charts/areachart'
import GridList from './elements/grids/ImageGridList'





export default () => (
    <Card>
    <GridList />

    
          <Link to="/templates">
           <Button variant="contained" color="primary"> Templates      </Button>
           </Link>
        <CardHeader title="Welcome" /> <CardContent>
        <PieChart />
        <LineChart />
        <AreaChart />
        </CardContent>
    </Card>
);
