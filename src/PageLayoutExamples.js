import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import classNames from 'classnames';
import { Link } from 'react-router-dom';

import { _rewriteUrlForNextExport } from 'next/router';

const styles = theme => ({
  item: {
    flexGrow: 1,
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  cardGrid: {
    padding: `${theme.spacing.unit * 8}px 0`,
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
  },
  cardContent: {
    flexGrow: 1,
  },
  cardMedia: {
    height: 0,
    paddingTop: '65%',
  },
});

const themes = [
  {
    name: 'Dashboard',
    description:
      'A minimal dashboard with taskbar and mini variant draw. ' +
      'The chart is courtesy of Recharts, but it is simple to substitute an alternative.',
    src: 'https://material-ui.com/static/images/layouts/dashboard.png',
    href: 'dashboard',
    source:
      'https://github.com/mui-org/material-ui/tree/master/docs/src/pagesdashboard',
  },
  {
    name: 'Sign-in',
    description: 'A simple sign-in page.',
    src: 'https://material-ui.com/static/images/layouts/sign-in.png',
    href: 'sign-in',
    source:
      'https://github.com/mui-org/material-ui/tree/master/docs/src/pagessign-in',
  },
  {
    name: 'Blog',
    description:
      'A sophisticated blog page layout. Markdown support is courtesy of markdown-to-jsx, ' +
      'but is easily replaced.',
    src: 'https://material-ui.com/static/images/layouts/blog.png',
    href: 'blog',
    source:
      'https://github.com/mui-org/material-ui/tree/master/docs/src/pagesblog',
  },
  {
    name: 'Checkout',
    description:
      'A step-by-step checkout page layout. ' +
      'Adapt the number of steps to suit your needs, or make steps optional.',
    src: 'https://material-ui.com/static/images/layouts/checkout.png',
    href: 'checkout',
    source:
      'https://github.com/mui-org/material-ui/tree/master/docs/src/pagescheckout',
  },
  {
    name: 'Album',
    description: 'A responsive album / gallery page layout with a hero unit and footer.',
    src: 'https://material-ui.com/static/images/layouts/album.png',
    href: 'album',
    source:
      'https://github.com/mui-org/material-ui/tree/master/docs/src/pagesalbum',
  },
  {
    name: 'Pricing',
    description:
      'Quickly build an effective pricing table for your potential customers with this page ' +
      'layout.',
    src: 'https://material-ui.com/static/images/layouts/pricing.png',
    href: 'pricing',
    source:
      'https://github.com/mui-org/material-ui/tree/master/docs/src/pagespricing',
  },
  {
    name: 'GridList',
    description:
      'Get a fancy grid in 1 minute ' +
      'layout.',
    src: 'http://qaru.site/img/5a487aa4c3ce26164174a7c823cd4b32.png',
    href: 'gridlist',
    source:
      'https://github.com/mui-org/material-ui/tree/master/docs/src/pagespricing',
  },
];

function PageLayoutExamples(props) {
  const { classes } = props;
  return (
    <div className={classNames(classes.layout, classes.cardGrid)}>
    <Link to="/">
     <Button variant="contained" color="primary"> Back      </Button>
     </Link>
    <Grid container spacing={16}>
      {themes.map(theme => (
        <Grid item sm={6} md={4} className={classes.item} key={theme.name}>
          <Card className={classes.card}>
          <Link to={theme.href}>
            <CardMedia
              component="a"
              className={classes.cardMedia}
              image={theme.src}
              title={theme.name}
              target="_blank"
            /></Link>
            <CardContent className={classes.cardContent}>
              <Typography gutterBottom variant="h5" align="left" component="h2">
                {theme.name}
              </Typography>
              <Typography component="p">{theme.description}</Typography>
            </CardContent>
            <CardActions>
              <Button component="a" href={theme.source} size="small" color="primary">
                Source code
              </Button>
            </CardActions>
          </Card>
        </Grid>
      ))}
    </Grid>
    </div>
  );
}

PageLayoutExamples.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PageLayoutExamples);
