const callApi = async (files, type, resource, params) => {
        let data = new FormData();
        files.map( file => {
          data.append('file', file.rawFile);
          });
          data.append('id', params.data._id);
          data.append('resource', resource);
        var fetchConf = { method: 'POST',
         headers: {},
         crossDomain:true,
         type: 'cors',
         body: data,
         id: params.data._id,
         cache: 'default' };


           fetch('http://localhost:3001/upload', fetchConf)
          .then(result => {
            if (result.status !== 200) return result
            result = result.json()
            console.log('CallApiresult', result);
      return result
          });





};





const addUploadFeature = requestHandler => (type, resource, params) => {


      if (type === 'UPDATE' || type === 'CREATE') {
      if (resource === 'posts') {
            if (params.data.gallery && params.data.gallery.length) {

              const formerPictures = params.data.gallery.filter(p => !(p.rawFile instanceof File));
              const newPictures = params.data.gallery.filter(p => p.rawFile instanceof File);


              return new Promise(function(resolve, reject) {
  callApi(newPictures, type, resource, params).then(resolved => {
    console.log('resolved result', resolved);

    requestHandler(type, resource, {
            ...params,
            data: {
                ...params.data,
                gallery: [],
            },
        })

  })
})




              // return  () => callApi(newPictures)
              //     .then(resolved_images => resolved_images.map((src, index) => ({
              //         src: src,
              //         title: `${newPictures[index].title}`,
              //     })))
              //     .then(transformedNewPictures => requestHandler(type, resource, {
              //         ...params,
              //         data: {
              //             ...params.data,
              //             gallery: [...transformedNewPictures, ...formerPictures],
              //         },
              //     }));



            }
      }
    }


    return requestHandler(type, resource, params);
};

export default addUploadFeature;
