import React from 'react';
import { Route } from 'react-router-dom';
import Register from './Register';
import Templates from './Templates';
import BlogPage from './templates/blog/Blog';
import AlbumPage from './templates/album/Album';
import CheckoutPage from './templates/checkout/Checkout';
import DashboardPage from './templates/dashboard/Dashboard';
import PricingPage from './templates/pricing/Pricing';
import SignInPage from './templates/sign-in/SignIn';
import Gridlist from './templates/gridlist/Gridlist';

export default [
    <Route exact path="/templates" component={Templates} noLayout />,
    <Route exact path="/gridlist" component={Gridlist} noLayout />,
    <Route exact path="/register" component={Register} noLayout />,
    <Route exact path="/blog" component={BlogPage} noLayout />,
    <Route exact path="/album" component={AlbumPage} noLayout />,
    <Route exact path="/checkout" component={CheckoutPage} noLayout />,
    <Route exact path="/dash" component={DashboardPage} noLayout />,
    <Route exact path="/pricing" component={PricingPage} noLayout />,
    <Route exact path="/signin" component={SignInPage} noLayout />,
];
