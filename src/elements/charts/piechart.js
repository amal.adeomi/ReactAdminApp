import React from 'react';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import {PieChart, Pie} from 'recharts';
import { Link } from 'react-router-dom';





const data = [{name: 'Group A', value: 400}, {name: 'Group B', value: 300},
                  {name: 'Group C', value: 300}, {name: 'Group D', value: 200},
                  {name: 'Group E', value: 278}, {name: 'Group F', value: 189}]




export default () => (
        <PieChart width={800} height={200}>
          <Pie startAngle={180} endAngle={0} data={data} dataKey="value" cx={200} cy={200} outerRadius={80} fill="#8884d8" label/>
        </PieChart>
);
