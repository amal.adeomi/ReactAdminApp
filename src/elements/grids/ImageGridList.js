import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Card from '@material-ui/core/Card';

// import tileData from './tileData';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: '100%',
  },
});



 const tileData = [
   {
     img: 'https://images.unsplash.com/photo-1547149600-a6cdf8fce60c?ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80',
     title: 'Image',
     author: 'author', component: 'Card',
     cols: 1,
   },
   {
     img: 'https://images.unsplash.com/photo-1547418278-4b272dcf0dd1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80',
     title: 'Image',
     author: 'author', component: 'Card',
     cols: 1,
   },
   {
     img: 'https://images.unsplash.com/photo-1547191220-ee44e430cabf?ixlib=rb-1.2.1&auto=format&fit=crop&w=588&q=80',
     title: 'Image',
     author: 'author', component: 'Card',
     cols: 1,
   },
   {
     img: 'https://images.unsplash.com/photo-1547157720-52d782e55b83?ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80',
     title: 'Image',
     author: 'author', component: 'Card',
     cols: 1,
   },
   {
     img: 'https://images.unsplash.com/photo-1547180905-c807ec5d3ea4?ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80',
     title: 'Image',
     author: 'author', component: 'Card',
     cols: 3,
   },
   {
     img: 'https://images.unsplash.com/photo-1547191043-3c1ae9bbfd39?ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80',
     title: 'Image',
     author: 'author', component: 'Card',
     cols: 1,
   },

   {
     img: 'https://images.unsplash.com/photo-1547139427-b5aa332ebfd7?ixlib=rb-1.2.1&auto=format&fit=crop&w=853&q=80',
     title: 'Image',
     author: 'author', component: 'Card',
     cols: 1,
   },
   {
     img: 'https://images.unsplash.com/photo-1547149666-769b42053e67?ixlib=rb-1.2.1&auto=format&fit=crop&w=564&q=80',
     title: 'Image',
     author: 'author', component: 'Card',
     cols: 2,
   },
   {
     img: 'https://images.unsplash.com/photo-1547157283-30f2b8141ed4?ixlib=rb-1.2.1&auto=format&fit=crop&w=675&q=80',
     title: 'Image',
     author: 'author', component: 'Card',
     cols: 1,
   },
   {
     img: 'https://images.unsplash.com/photo-1547087192-67947aaab6f4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80',
     title: 'Image',
     author: 'author', component: 'Card',
     cols: 1,
   },
   {
     img: 'https://images.unsplash.com/photo-1547149577-b976a4c8fab3?ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80',
     title: 'Image',
     author: 'author', component: 'Card',
     cols: 1,
   },
   {
     img: 'https://images.unsplash.com/photo-1547158944-a757fd11c576?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1211&q=80',
     title: 'Image',
     author: 'author', component: 'Card',
     cols: 1,
   },
   {
     img: 'https://images.unsplash.com/photo-1547349656-1a2929f7f759?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80',
     title: 'Image',
     author: 'author', component: 'Card',
     cols: 1,
   },
   {
     img: 'https://images.unsplash.com/photo-1547348906-9ec489d22339?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
     title: 'Image',
     author: 'author', component: 'Card',
     cols: 2,
   },
   {
     img: 'https://images.unsplash.com/photo-1547358497-cc9e35338632?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=676&q=80',
     title: 'Image',
     author: 'author', component: 'Card',
     cols: 2,
   },
 ];

function ImageGridList(props) {
  const { classes } = props;

  return (
    <div className={classes.root}>
      <GridList cellHeight={300} className={classes.gridList} cols={4}>
        {tileData.map(tile => (
          <GridListTile key={tile.img} cols={tile.cols || 1}>
            <Card>
              <img src={tile.img} alt={tile.title} width="100%" />
              //Write COmponent api here. So that if a function name is sent here, it s executed
            </Card>
          </GridListTile>
        ))  }
      </GridList>
    </div>
  );
}

ImageGridList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ImageGridList);
